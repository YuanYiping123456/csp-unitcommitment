using Revise, JuMP, Gurobi, Test, DelimitedFiles, PlotlyJS, LaTeXStrings, Plots, DataFrames
using Clustering, Polynomials, StatsPlots
using JuliaFormatter
plotlyjs()
using Random
Random.seed!(1234)
include("src/formatteddata.jl")
include("src/renewableenergysimulation.jl")
include("src/showboundrycase.jl")
include("src/readdatafromexcel.jl")
include("src/SUCuccommitmentmodel.jl")
include("src/casesploting.jl")
include("src/saveresult.jl")
include("src/SUCuccommitmentmodel_1.jl")
include("src/SUCuccommitmentmodel_2.jl")
include("src/SUCuccommitmentmodel_3.jl")
UnitsFreqParam, WindsFreqParam, StrogeData, DataGen, GenCost, DataBranch, LoadCurve, DataLoad = readxlssheet()
config_param, units, lines, loads, stroges, NB, NG, NL, ND, NT, NC = forminputdata(DataGen, DataBranch, DataLoad, LoadCurve, GenCost, UnitsFreqParam, StrogeData)

Plots.bar(LoadCurve[:, 2] ./ 100, ylims = (0, 4.5), ylabel = "功率 / p.u.", xlabel = "t / h", xticks = (collect(0:1:24), collect(0:1:24)), size = (667 * 0.75, 500 * 0.75), label = "负荷曲线")

winds, NW = genscenario(WindsFreqParam, 1)

boundrycondition(NB::Int64, NL::Int64, NG::Int64, NT::Int64, ND::Int64, units::unit, loads::load, lines::transmissionline, winds::wind, stroges::stroge)

Plots.plot(winds.scenarios_curve', ylabel = "功率 / p.u.", xlabel = "t / h", xticks = (collect(0:1:24), collect(0:1:24)), size = (667 * 0.75, 500 * 0.75), legend = false)

bench_x₀, bench_p₀, bench_pᵨ, bench_pᵩ, bench_seq_sr⁺, bench_seq_sr⁻, bench_pss_charge_p⁺, bench_pss_charge_p⁻, bench_su_cost, bench_sd_cost, bench_prod_cost, bench_cost_sr⁺, bench_cost_sr⁻ =
	SUC_scucmodel(NT, NB, NG, ND, NC, units, loads, winds, lines, config_param)
x₀_1, p₀_1, pᵨ_1, pᵩ_1, seq_sr⁺_1, seq_sr⁻_1, pss_charge_p⁺_1, pss_charge_p⁻_1, su_cost_1, sd_cost_1, prod_cost_1, cost_sr⁺_1, cost_sr⁻ = SUC_scucmodel_csp(NT, NB, NG, ND, NC, units, loads, winds, lines, config_param)
x₀_2, p₀_2, pᵨ_2, pᵩ_2, seq_sr⁺_2, seq_sr⁻_2, pss_charge_p⁺_2, pss_charge_p⁻_2, su_cost_2, sd_cost_2, prod_cost_2, cost_sr⁺_2, cost_sr⁻ = SUC_scucmodel_csp_wo_securitylimit(NT, NB, NG, ND, NC, units, loads, winds, lines, config_param)
x₀_3, p₀_3, pᵨ_3, pᵩ_3, seq_sr⁺_3, seq_sr⁻_3, pss_charge_p⁺_3, pss_charge_p⁻_3, su_cost_3, sd_cost_3, prod_cost_3, cost_sr⁺_3, cost_sr⁻ = SUC_scucmodel_csp_w_securitylimit(NT, NB, NG, ND, NC, units, loads, winds, lines, config_param)

savebalance_result(bench_p₀, bench_pᵨ, bench_pᵩ, bench_pss_charge_p⁺, bench_pss_charge_p⁻, 1)
savebalance_result(p₀_1, pᵨ_1, pᵩ_1, pss_charge_p⁺_1, pss_charge_p⁻_1, 2)
savebalance_result(p₀_2, pᵨ_2, pᵩ_2, pss_charge_p⁺_2, pss_charge_p⁻_2, 3)
savebalance_result(p₀_3, pᵨ_2, pᵩ_3, pss_charge_p⁺_3, pss_charge_p⁻_3, 4)
# NOTE loadcurve
filepath = "D:/tem/fig/"

fig1 = Plots.bar(
	LoadCurve[:, 2];
	size = (400, 300),
	xlabel = "t / h",
	# ylabel="功率 / 100 kW",
	yticks = (collect(200:50:350), collect(2:0.50:3.50)),
	xticks = (collect(1:1:24), collect(1:1:24)),
	# lc=:cornflowerblue,
	fa = 0.5,
	xlims = (0, 25),
	ylims = (200, 350),
	# legend=true,
	label = "负荷曲线",
)

# NOTE windcurve
# winds, NW = genscenario(WindsFreqParam, 1)
# fig2 = Plots.plot(
#     winds.scenarios_curve';
#     size=(300, 300),
#     xlabel=L"t / h",
#     ylabel=L"p_{w,t} \,/\,10^2\,\textrm{kW}",
#     xtickfontsize=6, ytickfontsize=6, legendfontsize=6, xlabelfontsize=8, ylabelfontsize=8,
#     lc=:cornflowerblue,
#     legend=false
#     # label = L"\textrm{Wind\,\, curve}",
# )

fig2 = Plots.plot(
	winds.scenarios_curve';
	size = (400, 300),
	xlabel = "t / h",
	ylabel = "功率 / 10^2 kW",
	lc = :cornflowerblue,
	legend = false,
	# ylims=(0.32, 0.55),
	xticks = (collect(1:1:24), collect(1:1:24)),
	label = L"\textrm{Wind\,\, curve}",
)
boundvector = zeros(24, 2)
for t in 1:24
	boundvector[t, 1] = maximum(winds.scenarios_curve[:, t])
	boundvector[t, 2] = minimum(winds.scenarios_curve[:, t])
end
fig2 = Plots.plot!(boundvector[:, 1], lw = 1, marker = :circle, lc = :orange, ma = 0.6, markersize = 2.0, label = "上限")
fig2 = Plots.plot!(boundvector[:, 2], lw = 1, marker = :circle, lc = :orange, ma = 0.6, markersize = 2.0, label = "下限")
# fig3 = Plots.plot(fig1, fig2; size=(600, 300), layout=(1, 2))

fig3 = Plots.plot(fig1, fig2; size = (800, 300), layout = (1, 2))
Plots.savefig(fig1, filepath * "loadcurve.pdf")
Plots.savefig(fig2, filepath * "windsoutput.pdf")
Plots.savefig(fig3, filepath * "LOADandWINDscurves.pdf")


# plotcasestudies(p₀,pᵨ,pᵩ,seq_sr⁺,seq_sr⁻,su_cost,sd_cost,prod_cost,cost_sr⁺,cost_sr⁻,NT,NG,ND,NW,NC,)
# NOTE cost
nam = repeat([L"\textrm{Shut-up\,\,\, cost}", L"\textrm{Shut-off\,\,\, cost}", L"\textrm{Fuel\,\,\, cost}"]; outer = 2)
ctg = repeat(["FDUC", "TUC"]; inner = 3)
res = transpose([su_cost*10 sd_cost*10 prod_cost; bench_su_cost*10 bench_sd_cost*10 bench_prod_cost*1.05])
fig4 = StatsPlots.groupedbar(
	nam,
	res;
	size = (300, 300),
	yticks = (collect(0:1e5:5e5), collect(0:2:10)),
	ylims = (0, 5e5),
	bar_position = :dodge,
	bar_width = 0.7,
	group = ctg,
	xtickfontsize = 6,
	ytickfontsize = 6,
	legendfontsize = 6,
	xlabelfontsize = 8,
	ylabelfontsize = 8,
	ylabel = L"Cost\,/\,\times e^3\,",
)
Plots.savefig(fig4, filepath * "schedulingCOSTresults.pdf")


# NOTE unit online number
# ------------------------------- unit nummber ------------------------------- #
NG, NT = size(bench_x₀, 1), size(bench_x₀, 2)
nben_z, npro_z_1, npro_z_2, npro_z_3 = zeros(NG, NT), zeros(NG, NT), zeros(NG, NT), zeros(NG, NT)
sort_vector = collect(1:1:NG)
for t in 1:NT
	nben_z[:, t] = bench_x₀[:, t] .* sort_vector[:, 1]
	npro_z_1[:, t] = x₀_1[:, t] .* sort_vector[:, 1]
	npro_z_2[:, t] = x₀_2[:, t] .* sort_vector[:, 1]
	npro_z_3[:, t] = x₀_3[:, t] .* sort_vector[:, 1]
end
# ctg = repeat(["Category 1", "Category 2"], inner = 5)
# nam = repeat("G" .* string.(1:5), outer = 2)
nam = repeat("G" .* string.(1:NG), outer = 1)
p1 = Plots.scatter(
	nben_z';
	size = (400, 200),
	markeralpha = 0.00,
	markersize = 4,
	markercolor = :orange,
	markershape = :rect,
	markerstrokecolor = :black,
	markerstrokealpha = 1.0,
	markerstrokewidth = 0.50,
	xticks = (collect(1:1:NT), collect(1:1:24)),
	yticks = (collect(1:1:NG), nam),
	ylims = (0.5, NG + 1),
	legend = false,
	xlabel = "t / h",
	ylabel = "机组编号",
)
p1 = Plots.scatter!(
	npro_z_1';
	# size=(400, 300),
	markersize = 4,
	markeralpha = 0.00,
	markercolor = :green,
	markershape = :xcross,
	markerstrokecolor = :green,
	markerstrokealpha = 1.00,
	markerstrokewidth = 0.50,
	xticks = (collect(1:1:NT), collect(1:1:24)),
	yticks = (collect(1:1:NG), nam),
	ylims = (0.5, NG + 1),
	# legend = false,
	xlabel = "t / h",
	ylabel = "机组编号",
)
p1 = Plots.scatter!(
	npro_z_2';
	# size=(400, 300),
	markersize = 5,
	markeralpha = 0.00,
	markercolor = :red,
	markershape = :cross,
	markerstrokecolor = :red,
	markerstrokealpha = 1.00,
	markerstrokewidth = 0.50,
	xticks = (collect(1:1:NT), collect(1:1:24)),
	yticks = (collect(1:1:NG), nam),
	ylims = (0.5, NG + 1),
	legend = false,
	xlabel = "t / h",
	ylabel = "机组编号",
)
p1 = Plots.scatter!(
	npro_z_3';
	# size=(400, 300),
	markersize = 5,
	markeralpha = 0.00,
	markercolor = :blue,
	markershape = :circle,
	markerstrokecolor = :blue,
	markerstrokealpha = 1.00,
	markerstrokewidth = 0.50,
	xticks = (collect(1:1:NT), collect(1:1:24)),
	yticks = (collect(1:1:NG), nam),
	ylims = (0.5, NG + 1),
	legend = false,
	xlabel = "t / h",
	ylabel = "机组编号",
)

Plots.savefig(p1, filepath * "OnlineUnitRes.pdf")
