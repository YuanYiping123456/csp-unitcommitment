    @constraint(
        scuc,
        [s = 1:NS, t = 1:NT, i = 1:NG],
        sum(sr⁺[(1+(s-1)*NG):(s*NG), t]) +
        sum(pc⁻[(NC*(s-1)+1):(s*NC), t]) >= 0.5 * units.p_max[i, 1] * x[i, t]
    )